/*
 * Podczerwien_tel.c
 *
 * Created: 23/11/2019 22:46:56
 * Author : Marcin
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

uint8_t licznik = 0;
uint8_t wykonaj = 0;
uint8_t impuls = 1;
uint8_t znak[11] = {250, 250, 250, 250, 250, 250, 250, 250, 250, 250, 250};
uint8_t i = 0;

int main(void)
{
    DDRD |= 0x00;
	PORTD |= (1 << PD2);
	DDRB |= 0xFF;
	/* Replace with your application code */
	TCCR0 |= (1 << CS01) | (1 << CS00);		//preskaler 64
	TIMSK |= (1 << TOIE0);					//uruchomienie flagi przerwan
	TCNT0 = 0x00;
	MCUCR |= (1 << ISC01);				//uruchomienie przerwania na sygnale opadajacym
	GICR |= (1 << INT0);				//umo�liwienie przerwania
	sei();
	
    while (1) 
    {
    }
}

ISR(INT0_vect)
{
	//TCNT0 = 0;
	MCUCR ^= (1 << ISC00);			//zmiana na inny sygnal wywolujacy przerwanie
	if(impuls == 1)
	{
		impuls++;
	}
	else if(impuls == 2 && (licznik == 4 && (TCNT0 >= 76 && TCNT0 <= 179)))		//minelo okolo 4,5 ms (tolerancja plus minus 0,2ms)
	{
		impuls++;
	}
	else if(impuls == 3 && (licznik == 4 && (TCNT0 >= 64 && TCNT0 <= 160)))		//4,25-4,65 ms
	{
		//PORTB ^= 0x02;
		//wykonaj = 1;
		impuls++;
	}
	else if(impuls >= 4 && impuls <= 36)
	{
		impuls++;
	}
	else if(impuls > 36 && licznik <= 10)		//doszlismy do kodowania znaku oraz gdy czas impulsu nie jest dluzszy ni� 10ms
	{
		if(licznik == 1 && (TCNT0 >= 127 && TCNT0 <= 178))			//dluzszy impuls 1,55 okolo
		{
			znak[i] = impuls - 37;
			i++;
		}
		else if(licznik == 0 && (TCNT0 >= 153 && TCNT0 <= 169))		//0,6- 0,66ms
		{
		}
		else if(licznik == 0 && (TCNT0 >= 124 && TCNT0 <= 139))		//0,49- 0,54ms
		{
		}
		if(znak[0] == 0 && znak[1] == 6 && znak[2] == 18 && !wykonaj)		//jezeli wcisnieto 5
		{
			PORTB ^= 0x10;
			wykonaj = 1;
		}
		else if(znak[0] == 0 && znak[1] == 4 && znak[2] == 6 && !wykonaj)	//wcisnieto 8
		{
			PORTB ^= 0x80;
			wykonaj = 1;
		}
		else if(znak[0] == 0 && znak[1] == 4 && znak[2] == 18 && !wykonaj)	//wcisnieto 2
		{
			PORTB ^= 0x02;
			wykonaj = 1;
		}
		else if(znak[0] == 4 && znak[1] == 16 && znak[2] == 18 && !wykonaj)		//wcisnieto 1
		{
			PORTB ^= 0x01;
			wykonaj = 1;
		}
		else if(znak[0] == 2 && znak[1] == 4 && znak[2] == 16 && !wykonaj)		//wcisnieto 3
		{
			PORTB ^= 0x04;
			wykonaj = 1;
		}
		else if(znak[0] == 6 && znak[1] == 16 && znak[2] == 18 && !wykonaj)		//wcisnieto 4
		{
			PORTB ^= 0x08;
			wykonaj = 1;
		}
		else if(znak[0] == 2 && znak[1] == 6 && znak[2] == 16 && !wykonaj)		//wcisnieto 6
		{
			PORTB ^= 0x20;
			wykonaj = 1;
		}
		else if(znak[0] == 4 && znak[1] == 6 && znak[2] == 16 && !wykonaj)		//wcisnieto 7
		{
			PORTB ^= 0x40;
			wykonaj = 1;
		}
		
		impuls++;
	}
	else
	{
		impuls = 1;
		i = 0;
	}
	
	
	/*if(licznik == 4	&& !wykonaj && (TCNT0 >= 76 && TCNT0 <= 179))	//minelo okolo 4,5 ms (tolerancja plus minus 0,2ms)
	{
		PORTB ^= 0x02;
		wykonaj = 1;
	}*/
	licznik = 0;
	TCNT0 = 0;
}

ISR(TIMER0_OVF_vect)
{
	licznik++;
	if(licznik >= 150)
	{
		if(wykonaj == 1)
		{
			for(uint8_t j = 0; j < 11; j++)
			{
				znak[j] = 250;
			}
		}
		wykonaj = 0;
		impuls = 1;
		i = 0;
	}
}
